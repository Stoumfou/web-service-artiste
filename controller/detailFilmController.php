<?php
    $film = simplexml_load_file ("http://mplasse.com/itpe/cinema/ws/film-".$_GET['id']);
?>
            <div class="row">
                <div class="col-lg-4 text-center v-center col-lg-offset-4">
                    <h2><?php echo $film['titre']?></h2>
                    <h3>Genre : <?php echo $film['genre']?></h3><br/>
                    <div style="text-align: left;">
                        Réalisateur : <?php echo "<a href='detailArtiste.php?id=".$film->realisateur['id']."'>".$film->realisateur['prenom']." ".$film->realisateur['nom']."</a>"?><br/><br/>
                        Acteur(s) : <br/>
                        <?php
                        $i = 0;
                        foreach ($film->acteurs->acteur as $acteur){
                            echo "- <a href='detailArtiste.php?id=".$acteur['id']."'>".$acteur['prenom']. " ".$acteur['nom']."</a><br/>";
                            $i++;
                        }
                        if ($i == 0){
                            echo "Aucun acteur !<br/>";
                        }
                        ?>
                    </div>
                </div>
            </div>