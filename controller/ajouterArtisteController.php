<?php
/**
 * Created by PhpStorm.
 * User: Alexis
 * Date: 19/01/2015
 * Time: 23:22
 */
    // Inclure la bibliotheque
    require("../Requests-master/library/Requests.php");
    // Charger les classes internes à Requests
    Requests::register_autoloader();

?>
<div class="row">
    <div class="col-lg-4 text-center v-center col-lg-offset-4">
        <?php
        if(isset($_POST['nom'],$_POST['prenom']))
        {
            if(empty($_POST['nom']) || empty($_POST['prenom']))
            {?><br/>
                <div class="alert alert-danger" role="alert">
                    <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                    <span class="sr-only">Error:</span>
                    Des champs sont vides, Veuillez réessayer.
                </div>
            <?php
            } else
            {
                $nom = htmlentities($_POST['nom']);
                $prenom = htmlentities($_POST['prenom']);

                // Requête POST ------------------
                $url = "http://www.mplasse.com/itpe/cinema/ws/artistes";
                // en-tetes de la requete
                $headers = array();
                // Paramètres à transmettre
                $data = array("nom" =>  $nom, "prenom" => $prenom);
                // Authentification
                $login = "admin";
                $password = "admin";
                $options = array("auth" => new Requests_Auth_Basic(array($login, $password)));
                // Appel
                $response = Requests::post($url, $headers, $data, $options);
                $code = $response->status_code;
                //$body = $response->body;
                ?>
                <br/>
                <div class="alert alert-success" role="alert">
                <h2>
                    <?php
                   // echo ("<p>POST sur ".$url.", avec prenom=".$data['prenom']." & nom=".$data['nom']." Mon code : ".$code." </p> ");
                    switch($code)
                    {
                        case 201:
                            echo "<p id='ajoutArtiste' >L'artiste : <strong>".$nom." ".$prenom."</strong><br/>a bien été créé </p>";
                            break;

                        case 409:
                            echo "<p>Attention il y'a un conflict; vérifiez que l'artiste <strong>".$nom." ".$prenom."</strong> n'existe pas déjà et recommencez !!!</p>";
                            break;
                    }
                    ?>
                </h2>
                </div><?php
            }
        } else
        {?><br/>
            <div class="alert alert-danger" role="alert">
                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                <span class="sr-only">Error:</span>
                Erreur d'ajouts d'artiste, Veuillez réessayer.
            </div>
        <?php
        }
        ?>
    </div>
</div>