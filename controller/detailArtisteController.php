<?php
    $artiste = simplexml_load_file ("http://mplasse.com/itpe/cinema/ws/artiste-".$_GET['id']);
?>
            <div class="row">
                <div class="col-lg-4 text-center v-center col-lg-offset-4">
                    <h2><?php echo $artiste['prenom']." ".$artiste['nom']?></h2>
                    <div style="text-align: left;">
                        A réalisé : <br/>
                        <?php
                        $i = 0;
                        foreach ($artiste->realisateurDe->film as $film){
                            echo "- <a href='detailFilm.php?id=".$film['id']."'>".$film['titre']."</a><br/>";
                            $i++;
                        }
                        if ($i == 0){
                            echo "Aucun film !<br/>";
                        }
                        ?>
                        <br/>
                        A joué dans : <br/>
                        <?php
                        $j = 0;
                        foreach ($artiste->roles->role as $role){
                            echo "- <a href='detailFilm.php?id=".$role['id_film']."'>".$role['titre']."</a><br/>";
                            $j++;
                        }
                        if ($j == 0){
                            echo "Aucun film !<br/>";
                        }
                        ?>
                    </div>
                </div>
            </div>