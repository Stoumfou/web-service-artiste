            <div class="row">
                <div class="col-lg-4 text-center v-center col-lg-offset-4">
                    <h2>Artistes trouvés : </h2>
                        <table style="width: 100%;">
                            <tr>
                                <th>Nom</th>
                                <th>Modifier</th>
                                <th>Supprimer</th>
                            </tr>
                            <?php
                            $xml = simplexml_load_file ("http://www.mplasse.com/itpe/cinema/ws/artistes?nom=".$_POST['name']."&nb=15&noPage=1");
                            foreach ($xml->artiste as $artiste){ ?>
                                <tr>
                                    <td><?php echo "<a href='detailArtiste.php?id=".$artiste['id']."'>".$artiste['prenom']." ".$artiste['nom']."</a>"?></td>
                                    <td><?php echo "<a href='editArtiste.php?id=".$artiste['id']."'><img src='../images/edit.gif'/></a>"?></td>
                                    <td><?php echo "<a href='deleteArtiste.php?id=".$artiste['id']."'><img src='../images/delete.gif'/></a>"?></td>
                                </tr>
                            <?php
                            }
                            if (count($xml->artiste) == 0) {
                                echo "Aucun artiste de ce nom-là !";
                            }
                            ?>
                        </table>
                </div>
            </div>
        <?php 