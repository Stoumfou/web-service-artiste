            <div class="row">
                <div class="col-lg-6 text-center v-center col-lg-offset-3">
                    <h2>Films trouvés : </h2>
                        <table style="width: 100%;">
                            <tr>
                                <th>Titre</th>
                                <th>Genre</th>
                                <th>Modifier</th>
                                <th>Supprimer</th>
                            </tr>
                            <?php
                            $xml = simplexml_load_file ("http://mplasse.com/itpe/cinema/ws/films?titre=".$_POST['name']."&realisateur=".$_POST['realisateur']."&nb=15&noPage=1");
                            foreach ($xml->film as $film){ ?>
                                <tr>
                                    <td><?php echo "<a href='detailFilm.php?id=".$film['id']."'>".$film['titre']."</a>"?></td>
                                    <td><?php echo $film['genre']?></td>
                                    <td><?php echo "<a href='editFilm.php?id=".$film['id']."'><img src='../images/edit.gif'/></a>"?></td>
                                    <td><?php echo "<a href='deleteFilm.php?id=".$film['id']."'><img src='../images/delete.gif'/></a>"?></td>
                                </tr>
                            <?php
                            }
                            if (count($xml->film) == 0) {
                                echo "Aucun film ne correspond !";
                            }
                            ?>
                        </table>
                </div>
            </div>
        <?php 