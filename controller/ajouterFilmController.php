<?php
/**
 * Created by PhpStorm.
 * User: Alexis
 * Date: 19/01/2015
 * Time: 20:34
 */
?>
    <div class="row">
        <div class="col-lg-4 text-center v-center col-lg-offset-4">
            <?php
            if(isset($_POST['nomFilm'],$_POST['nomRealisateur'],$_POST['genre']))
            {
                if(empty($_POST['nomFilm']) || empty($_POST['nomRealisateur']) || empty($_POST['genre']))
                {?><br/>
                    <div class="alert alert-danger" role="alert">
                        <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                        <span class="sr-only">Error:</span>
                        Des champs sont vides, Veuillez réessayer.
                    </div>
                <?php
                } else
                {
                    $nomFilm = htmlentities($_POST['nomFilm']);
                    $nomRealisateur = htmlentities($_POST['nomRealisateur']);
                    $genre = htmlentities($_POST['genre']);
                    switch($genre)
                    {
                        case 'C':
                            $nomGenre='Comédie';
                            break;
                        case 'O':
                            $nomGenre='Comédie dramatique';
                            break;
                        case 'D':
                            $nomGenre='Drame';
                            break;
                        case 'J':
                            $nomGenre='Jeunesse';
                            break;
                        case 'P':
                            $nomGenre='Policier';
                            break;
                    }

                    //lel
                    $xml = simplexml_load_file ("http://mplasse.com/itpe/cinema/ws/films?titre=".$nomFilm."&realisateur=".
                    $nomRealisateur."&id_genre=".$genre);

                    ?>
                    <br/>
                    <div class="alert alert-success" role="alert">
                        <h2><?php echo "Le film : <strong>". $nomFilm ."</strong><br/>".
                                "Réalisé par : <strong>".$nomRealisateur. "</strong><br/>" .
                                "Du genre : <strong>".$nomGenre."</strong><br/>".
                                "a bien été créé"?></h2>
                    </div><?php
                }
            } else
            {?><br/>
                <div class="alert alert-danger" role="alert">
                    <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                    <span class="sr-only">Error:</span>
                    Erreur d'ajouts du film, Veuillez réessayer.
                </div>
            <?php
            }
            ?>
        </div>
    </div>