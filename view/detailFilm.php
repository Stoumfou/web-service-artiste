<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <title>ItParisArtiste - Recherche</title>
        <meta name="generator" content="Bootply" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <link href="../css/bootstrap.min.css" rel="stylesheet">

    <!--[if lt IE 9]>
    <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <link href="../css/styles.css" rel="stylesheet">
</head>


<body>

<div class="container-full">
    
<?php include_once("header.php");?>

<?php
$action = $_POST['action'];
switch($action){
    case 'editFilm':{
        include_once("../controller/editFilmConfirmController.php");
        break;
    }
}
?>
      
<?php include("../controller/detailFilmController.php");?>
    
<?php include_once("footer.php");?>
    
</div>


<!-- script references -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
</body>
</html>