<!DOCTYPE html>
<html lang="fr">
<head>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
	<meta charset="utf-8">
	<title>ItParisArtiste - Ajouter</title>
	<meta name="generator" content="Bootply" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<link href="../css/bootstrap.min.css" rel="stylesheet">

	<!--[if lt IE 9]>
	<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <script src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script src="../js/ajout.js"></script>
	<![endif]-->
	<link href="../css/styles.css" rel="stylesheet">
</head>

<body>
<div class="container-full">

	<?php include_once("header.php");?>

	<div class="row">
		<div class="col-lg-4 text-center v-center col-lg-offset-4">

			<input type="radio" name="rechercheType" value="Artiste"> Artiste
			<input type="radio" name="rechercheType" value="Film" checked> Film

			<div id="ajoutsFilm"><form role="form" action="ajouter.php" method="POST">
					<h1>Ajouter un film</h1><br/>
					<div class="form-group">
						<span class="glyphicon glyphicon-film" aria-hidden="true"></span>
						<label for="nomFilm">Titre : </label>
						<input type="text" class="form-control" name="nomFilm" id="nomFilm" placeholder="Titre du film"/>
					</div>
					<div class="form-group">
						<span class="glyphicon glyphicon-user" aria-hidden="true"></span>
						<label for="nomRealisateur">Réalisateur : </label>
						<input type="text" class="form-control" name="nomRealisateur" id="nomRealisateur" placeholder="Nom du réalisateur"/>
					</div>
					<div class="form-group">
						<label for="genre">Genre : </label>
						<select name="genre" id="genre" class="form-control">
							<option value="C">Comédie</option>
							<option value="O">Comédie dramatique</option>
							<option value="D">Drame</option>
							<option value="J">Jeunesse</option>
							<option value="P">Policier</option>
						</select>
					</div>
					<input type="hidden" name="action" value="ajouterFilm"><br/>
					<button type="submit" class="btn btn-primary">Ajouter</button>
			</form></div>

		<div id="ajoutsArtiste" style="display: none;"><form role="form" action="ajouter.php" method="POST">
				<h1>Ajouter un Artiste</h1><br/>
				<div class="form-group">
					<span class="glyphicon glyphicon-user" aria-hidden="true"></span>
					<label for="nom">Nom : </label>
					<input type="text" class="form-control" name="nom" id="nom" placeholder="Nom de l'artiste"/>
				</div>
				<div class="form-group">
					<span class="glyphicon glyphicon-user" aria-hidden="true"></span>
					<label for="prenom">Prénom : </label>
					<input type="text" class="form-control" name="prenom" id="prenom" placeholder="Prénom de l'artiste"/>
				</div>
				<input type="hidden" name="action" value="ajouterArtiste"><br/>
				<button type="submit" class="btn btn-primary">Ajouter</button>
		</div>
		</form></div>

</div>
<!-- /row -->
<?php
    $action = (isset($_POST['action']) and $_POST['action'] != '' ) ? $_POST['action'] : "";
switch($action){
	case 'ajouterFilm':{
		include_once("../controller/ajouterFilmController.php");
		break;
	}
	case 'ajouterArtiste':{
		include_once("../controller/ajouterArtisteController.php");
		break;
	}
}
?>
<?php include_once("footer.php");?>
<!-- /container -->
</div>


<!-- script references -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
</body>
</html>