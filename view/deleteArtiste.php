<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <title>ItParisArtiste - Recherche</title>
        <meta name="generator" content="Bootply" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <link href="../css/bootstrap.min.css" rel="stylesheet">

    <!--[if lt IE 9]>
    <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <link href="../css/styles.css" rel="stylesheet">
</head>
<div class="container-full">
    
<?php include_once("header.php");?>


<?php include("../controller/deleteArtisteController.php"); ?>

	<div class="row">
			<div class="col-lg-4 text-center v-center col-lg-offset-4">
				<?php if( $code == 204 ) echo "<h2> L'artiste d'identifiant [".$_GET['id']."] a bien été supprimé. </h2>"; ?>
			</div>
	</div>

<?php include_once("footer.php");?>    
</div>


<!-- script references -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
</body>
</html>