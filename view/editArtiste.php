<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <title>ItParisArtiste - Modification d'artiste</title>
        <meta name="generator" content="Bootply" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <link href="../css/bootstrap.min.css" rel="stylesheet">

    <!--[if lt IE 9]>
    <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <link href="../css/styles.css" rel="stylesheet">
</head>


<body>

<div class="container-full">
	<?php include_once("header.php");

	$code ="";//var_dump($_POST);
	$nom = (isset($_POST['nom']) and $_POST['nom'] != '' ) ?  $_POST['nom'] : "" ;
	$prenom = (isset($_POST['prenom']) and $_POST['prenom'] != '' ) ?  $_POST['prenom'] : "" ;
	$id = (isset($_POST['id']) and $_POST['id'] != '' ) ?  $_POST['id'] : "" ;
	
	//if( (!empty(isset($_POST('nom'))) and $_POST('nom') !='' )  or ( !empty(isset($_POST('prenom'))) and $_POST('prenom') !='') )
	if( !empty($nom)  or !empty($prenom) )
	{
		include_once("../controller/modifierArtisteController.php");
	}
	else //if( (isset($_GET('id')) and $_GET('id') !='') )
	{	 
		$url ="http://www.mplasse.com/itpe/cinema/ws/artiste-".$_GET['id']."";
		$id = $_GET['id'];
		$xml = simplexml_load_file($url);//simplexml_load_string("<<<XML ".$url." XML");//
		
		foreach( $xml->attributes() as $cle=>$valeur)
		{
			//echo "<br/><strong> $cle : $valeur </strong>";
			if( $cle == 'nom') $nom = $valeur;
			if( $cle == 'prenom' ) $prenom = $valeur;
		}
	}
	
	?>
		  
	<div class="row">
		<div class="col-lg-4 text-center v-center col-lg-offset-4">
			<?php if( $code == 204 ) echo "<h2>Modification effectu�e.</h2>"; ?>
			<div id ="searchArtiste">
				<form class="col-lg-12" action="editArtiste.php" method="post">
				<h1>Modifier un artiste</h1><br/>
				<div class="form-group">
					<span class="glyphicon glyphicon-user" aria-hidden="true"></span>
					<label for="nomArtiste">Nom : </label>
					<input type="text" class="form-control" id="nomArtiste" placeholder="Nom de l'artiste" name="nom" value="<?php echo $nom; ?>" >
					<input type="hidden" name="id" <?php echo "value=\"".$id."\"" ?> ><br/>
					<label for="prenomArtiste">Prenom : </label>
					<input type="text" class="form-control" id="prenomArtiste" placeholder="Prenom de l'artiste" name="prenom" value="<?php echo $prenom; ?>">
					<br/>
					<span class="input-group-btn">
						<button class="btn btn-lg btn-primary" type="submit">Modifier</button>
					</span>
				</div>
				</form>
			</div>
			
		</div>
		
		<?php include_once("footer.php");?>
		
	</div>
	
	
</div>
<!-- script references -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
</body>
</html>