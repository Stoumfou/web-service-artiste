<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <title>ItParisArtiste - Recherche</title>
        <meta name="generator" content="Bootply" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <link href="../css/bootstrap.min.css" rel="stylesheet">

    <!--[if lt IE 9]>
    <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <link href="../css/styles.css" rel="stylesheet">
</head>


<body>

<div class="container-full">
    
<?php include_once("header.php");?>
      
<div class="row">
    <div class="col-lg-4 text-center v-center col-lg-offset-4">
        <input type="radio" name="searchType" value="Artiste" checked> Artiste
        <input type="radio" name="searchType" value="Film"> Film
        <div id ="searchArtiste"><form class="col-lg-12" action="rechercher.php" method="post">
            <h1>Rechercher un artiste</h1><br/>
            <div class="form-group">
                <span class="glyphicon glyphicon-user" aria-hidden="true"></span>
                <label for="nomArtiste">Nom : </label>
                <input type="text" class="form-control" id="nomArtiste" placeholder="Nom de l'artiste" name="name">
                <input type="hidden" name="action" value="rechercheArtiste"><br/>
                    <span class="input-group-btn">
                        <button class="btn btn-lg btn-primary" type="submit">Rechercher</button>
                    </span>
	        </div>
        </form></div>
        <div id ="searchFilm" style="display: none;"><form class="col-lg-12" action="rechercher.php" method="post">
            <h1>Rechercher un film</h1><br/>
            <div class="form-group">
                <span class="glyphicon glyphicon-user" aria-hidden="true"></span>
                <label for="titreFilm">Titre : </label>
                <input type="text" class="form-control" id="titreFilm" placeholder="Titre du film" name="name">
                <label for="realisateurFilm">Réalisateur : </label>
                <input type="text" class="form-control" id="realisateurFilm" placeholder="Réalisateur du film" name="realisateur">
                <input type="hidden" name="action" value="rechercheFilm"><br/>
                    <span class="input-group-btn">
                        <button class="btn btn-lg btn-primary" type="submit">Rechercher</button>
                    </span>
            </div>
        </form></div>
    </div>
</div>
    
<?php
    $action = (isset($_POST['action']) and $_POST['action'] != '' ) ? $_POST['action'] : "";
    switch($action){
        case 'rechercheArtiste':{
            include_once("../controller/rechercheArtisteController.php");
            break;
        }
        case 'rechercheFilm':{
            include_once("../controller/rechercheFilmController.php");
            break;
        }
    }
?>
    
<?php include_once("footer.php");?>
    
</div>


<!-- script references -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
</body>
</html>